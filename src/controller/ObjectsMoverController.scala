package controller

import model.characters._
import model.objects.GameObject
import model.objects.Coin
import scala.collection.mutable.ListBuffer

/**
  * Created by Giacomo on 13/03/2017.
  */

class ObjectsMoverController {

  def moveEnemies(enemies: ListBuffer[AbstractEnemy]): Unit = enemies foreach (enemy => enemy.move())

  def movePieces(coins: ListBuffer[Coin]): Unit = coins foreach (coin => coin.move())

  def moveObjects(objects: ListBuffer[GameObject]): Unit = objects foreach (obj=>obj.move())

}
