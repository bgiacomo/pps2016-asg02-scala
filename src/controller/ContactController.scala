package controller

import model.objects.Coin
import model.objects.GameObject
import utils.Res
import view.Audio
import view.Main
import view.Score
import model.characters._
import scala.collection.mutable.ListBuffer


class ContactController() {

  private[this] val COIN_INCREMENT: Int = 50

  def checkContactWithCoin(coins: ListBuffer[Coin], mario: Mario): Unit = {

    (0 to coins.size-2) map { i =>
      if (mario.contactCoin(coins(i))) {
        Audio.playSound(Res.AUDIO_MONEY)
        Score.getInstance.incrementScore(COIN_INCREMENT)
        Main.label.setText("  Score:  " + Score.getInstance.getScore)
        coins-=coins(i)
      }
    }
  }

  def checkContactsWithObjects(objects: ListBuffer[GameObject], mario: Mario,enemies: ListBuffer[AbstractEnemy]) {

    objects foreach (obj => {
      enemies foreach ((enemy: AbstractEnemy) => {
        if (enemy.isNearby(obj))
          enemy.contact(obj)
        if (mario.isNearby(obj))
          mario.contact(obj)
      })
    })
  }


  def checkContactAmongstCharacters(mario: Mario,turtle: Turtle, mushroom: Mushroom, skull: Skull,bird: Bird) {

    if (mario.isNearby(bird)) {
      mario.contact(bird)
    }
    if (mario.isNearby(mushroom)) {
      mario.contact(mushroom)
    }
    if (mario.isNearby(turtle)) {
      mario.contact(turtle)
    }
    if (mario.isNearby(skull)) {
      mario.contact(skull)
    }
    if (mushroom.isNearby(turtle)) {
      mushroom.contact(turtle)
    }

    if (skull.isNearby(mushroom)) {
      skull.contact(mushroom)
    }
    if (turtle.isNearby(skull)) {
      turtle.contact(skull)
    }
    if (bird.isNearby(skull)) {
      bird.contact(skull)
    }
    if (bird.isNearby(mushroom)) {
      bird.contact(mushroom)
    }
    if (bird.isNearby(turtle)) {
      bird.contact(turtle)
    }
  }
}