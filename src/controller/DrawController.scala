package controller

import java.awt._
import model.objects._
import model.characters._
import utils.Res
import model.Background
import model.Castle
import model._
import scala.collection.mutable.ListBuffer


object DrawController {
  private val TURTLE_FREQUENCY: Int = 45
  private val TURTLE_DEAD_OFFSET_Y: Int = 30
  private val SKULL_DEAD_OFFSET_Y: Int = 10
  private val SKULL_FREQUENCY: Int = 45
  private val BIRD_DEAD_OFFSET_Y: Int = 10
  private val BIRD_FREQUENCY: Int = 45
  private val MUSHROOM_DEAD_OFFSET_Y: Int = 20
  private val MUSHROOM_FREQUENCY: Int = 45
  private val MARIO_FREQUENCY: Int = 25
  private val CASTLE_X_POS: Int = 4850
  private val CASTLE_Y_POS: Int = 145
  private val FLAG_Y_POS: Int = 115
  private val FLAG_X_POS: Int = 4650
  private val CASTLE_INITIAL_POSITION_X: Int = 10
  private val CASTLE_INITIAL_POSITION_Y: Int = 95
  private val STARTING_POINT_INITIAL_POSITION_X: Int = 220
  private val STARTING_POINT_INITIAL_POSITION_Y: Int = 234
}

class DrawController() {

  def drawEnemy(g2: Graphics, abstractEnemy: AbstractEnemy): Unit ={
    if(abstractEnemy.alive){
      abstractEnemy match {
        case abstractEnemy: Turtle => g2.drawImage(abstractEnemy.walk(Res.IMGP_CHARACTER_TURTLE, DrawController.TURTLE_FREQUENCY), abstractEnemy.getX, abstractEnemy.getY, null)
        case abstractEnemy: Mushroom => g2.drawImage(abstractEnemy.walk(Res.IMGP_CHARACTER_MUSHROOM, DrawController.MUSHROOM_FREQUENCY), abstractEnemy.getX, abstractEnemy.getY, null)
        case abstractEnemy: Skull => g2.drawImage(abstractEnemy.walk(Res.IMGP_CHARACTER_SKULL, DrawController.SKULL_FREQUENCY), abstractEnemy.getX, abstractEnemy.getY, null)
        case abstractEnemy: Bird => g2.drawImage(abstractEnemy.walk(Res.IMGP_CHARACTER_BIRD, DrawController.BIRD_FREQUENCY), abstractEnemy.getX, abstractEnemy.getY, null)
      }
    }else {
      var customImg:Int = 0
      abstractEnemy match {
        case abstractEnemy: Turtle => customImg = DrawController.TURTLE_DEAD_OFFSET_Y
        case abstractEnemy: Mushroom => customImg = DrawController.MUSHROOM_DEAD_OFFSET_Y
        case abstractEnemy: Skull => customImg = DrawController.SKULL_DEAD_OFFSET_Y
        case abstractEnemy: Bird => customImg = DrawController.BIRD_DEAD_OFFSET_Y
      }
      g2.drawImage(abstractEnemy.deadImage, abstractEnemy.x, abstractEnemy.getY +customImg, null)
    }
  }

  def drawMario(g2: Graphics, mario: Mario) {
    if (mario.jumping) g2.drawImage(mario.doJump(), mario.getX, mario.getY, null)
    else g2.drawImage(mario.walk(Res.IMGP_CHARACTER_MARIO, DrawController.MARIO_FREQUENCY), mario.getX, mario.getY, null)
  }

  def drawGameObject(g2: Graphics, gameObject: GameObject, xPosition: Int): Unit ={
      gameObject match {
        case gameObject: Background => g2.drawImage(gameObject.image, gameObject.getX, 0, null)
        case gameObject: Castle => g2.drawImage(gameObject.image, DrawController.CASTLE_INITIAL_POSITION_X - xPosition, DrawController.CASTLE_INITIAL_POSITION_Y, null)
        case gameObject: StartPoint => g2.drawImage(gameObject.image, DrawController.STARTING_POINT_INITIAL_POSITION_X - xPosition, DrawController.STARTING_POINT_INITIAL_POSITION_Y, null)
        case gameObject: FinalCastle => g2.drawImage(gameObject.image, DrawController.CASTLE_X_POS - xPosition, DrawController.CASTLE_Y_POS, null)
        case gameObject: Flag => g2.drawImage(gameObject.image, DrawController.FLAG_X_POS - xPosition, DrawController.FLAG_Y_POS, null)
      }
  }

  def drawPieces(g2: Graphics, coins: ListBuffer[Coin]): Unit = coins foreach (coin=> g2.drawImage(coin.imageOnMovement, coin.getX, coin.getY, null))

  def drawObjects(g2: Graphics, objects: ListBuffer[GameObject]): Unit =objects foreach (obj => g2.drawImage(obj.getImageObject,obj.x,obj.y,null))

}