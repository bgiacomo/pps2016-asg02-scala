package controller

import model.objects._
import scala.collection.mutable.ListBuffer


class ObjectsHandler {

  import ObjectsHandlerHelper._

  var objects: ListBuffer[GameObject] = _

  init()

  def init(): Unit ={
    objects=new ListBuffer[GameObject]
    addTunnels()
    addBlocks()
  }

  private[this] object ObjectsHandlerHelper {

    def addTunnels(): Unit = {
      objects.+=(new Tunnel(600, 230))
      objects.+=(new Tunnel(1000, 230))
      objects.+=(new Tunnel(1600, 230))
      objects.+=(new Tunnel(1900, 230))
      objects.+=(new Tunnel(2500, 230))
      objects.+=(new Tunnel(3000, 230))
      objects.+=(new Tunnel(3800, 230))
      objects.+=(new Tunnel(4500, 230))
    }

    def addBlocks(): Unit = {
      objects.+=(new Block(400, 180))
      objects.+=(new Block(1200, 180))
      objects.+=(new Block(1270, 170))
      objects.+=(new Block(1340, 160))
      objects.+=(new Block(2000, 180))
      objects.+=(new Block(2600, 160))
      objects.+=(new Block(2650, 180))
      objects.+=(new Block(3500, 160))
      objects.+=(new Block(3550, 140))
      objects.+=(new Block(4000, 170))
      objects.+=(new Block(4200, 200))
      objects.+=(new Block(4300, 210))
    }

  }

}
