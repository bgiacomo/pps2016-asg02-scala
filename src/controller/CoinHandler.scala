package controller


import model.objects.Coin
import scala.collection.mutable.ListBuffer

class CoinHandler {

  var coins: ListBuffer[Coin] = _

  this.createCoins()

  private[this] def createCoins(): Unit = {
    coins=new ListBuffer[Coin]
    this.coins.+=(new Coin(402, 145))
    this.coins.+=(new Coin(1202, 140))
    this.coins.+=(new Coin(1272, 95))
    this.coins.+=(new Coin(1342, 40))
    this.coins.+=(new Coin(1650, 145))
    this.coins.+=(new Coin(2650, 145))
    this.coins.+=(new Coin(3000, 135))
    this.coins.+=(new Coin(3400, 125))
    this.coins.+=(new Coin(4200, 145))
  }

}

