package model.characters

import java.awt.Image
import utils.Res
import utils.Utils

object Turtle {
  private val WIDTH: Int = 43
  private val HEIGHT: Int = 50
}

class Turtle( x: Int,  y: Int) extends AbstractEnemy(x, y, Turtle.WIDTH, Turtle.HEIGHT) {
  final def deadImage: Image = {
    Utils.getImage(Res.IMG_TURTLE_DEAD)
  }
}

object Mushroom {
  private val WIDTH: Int = 27
  private val HEIGHT: Int = 30
}

class Mushroom( x: Int,  y: Int) extends AbstractEnemy(x, y, Mushroom.WIDTH, Mushroom.HEIGHT) {
  final def deadImage: Image = {
    Utils.getImage(if (this.toRight) Res.IMG_MUSHROOM_DEAD_DX
    else Res.IMG_MUSHROOM_DEAD_SX)
  }
}

object Skull {
  private val WIDTH: Int = 37
  private val HEIGHT: Int = 53
}

class Skull( x: Int,  y: Int) extends AbstractEnemy(x, y, Skull.WIDTH, Skull.HEIGHT) {
  final def deadImage: Image = {
    Utils.getImage(Res.IMG_SKULL_DEAD_SX)
  }
}

object Bird {
  private val WIDTH: Int = 35
  private val HEIGHT: Int = 35
}

class Bird( x: Int,  y: Int) extends AbstractEnemy(x, y, Bird.WIDTH, Bird.HEIGHT) {
  final def deadImage: Image = {
    Utils.getImage(if (this.toRight) Res.IMG_BIRD_DEAD_SX
    else Res.IMG_BIRD_DEAD_DX)
  }
}
