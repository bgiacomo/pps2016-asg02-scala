package model.characters

import java.awt._
import model.objects.GameObject
import java.util.logging.Level
import java.util.logging.Logger

object AbstractEnemy {
  private val LOGGER: Logger = Logger.getLogger(classOf[AbstractEnemy].getName)
  private val PAUSE: Int = 15
  private val OFFSET_X: Int = 1
}

abstract class AbstractEnemy(x: Int,  y: Int,  width: Int, height: Int) extends BasicCharacter(x, y, width, height) with Runnable {
  toRight=true
  moving=true
  offsetX = AbstractEnemy.OFFSET_X
  startThread()
  var offsetX: Int = 0

  private def startThread() {
    val chronoEnemy: Thread = new Thread(this)
    chronoEnemy.start()
  }

  override def move() {
    this.offsetX = if (toRight) AbstractEnemy.OFFSET_X
    else -AbstractEnemy.OFFSET_X
    this.setX(this.getX + this.offsetX)
  }

  def run(): Unit = {
    while (true) {
      {if (alive) move()
        try { Thread.sleep(AbstractEnemy.PAUSE) }
        catch { case e: InterruptedException =>  AbstractEnemy.LOGGER.log(Level.WARNING, "AbstractEnemy exception!", e) }
      }
    }
  }

  def contact(gameObject: GameObject): Unit = {
    if (hitAhead(gameObject) && toRight) {
      toRight=false
      offsetX = -AbstractEnemy.OFFSET_X
    }
    else if (hitBack(gameObject) && !toRight) {
      toRight=true
      offsetX = AbstractEnemy.OFFSET_X
    }
  }

  def contact(character: BasicCharacter): Unit = {
    if (hitAhead(character) && toRight)
      toRight=false
    else if (hitBack(character) && !toRight)
      toRight=true

    offsetX = AbstractEnemy.OFFSET_X
  }

  def deadImage: Image
}