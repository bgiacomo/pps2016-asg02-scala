package model.characters

import java.awt._
import model.objects.Coin
import view.Main
import model.objects.GameObject
import utils.Res
import utils.Utils
import view.Score

object Mario {
  private val MARIO_OFFSET_Y_INITIAL: Int = 243
  private val FLOOR_OFFSET_Y_INITIAL: Int = 293
  private val WIDTH: Int = 28
  private val HEIGHT: Int = 50
  private val HEIGHT_LIMIT: Int = 0
  private val JUMPING_LIMIT: Int = 42
  private val JUMP_OFFSET: Int = 4
  private val JUMPING_EXTENT: Int = 0
  private val HIT_INCREMENT: Int = 10
}

class Mario( x: Int,  y: Int) extends BasicCharacter(x, y, Mario.WIDTH, Mario.HEIGHT) {

  var jumping: Boolean = false
  var jumpingExtent: Int = Mario.JUMPING_EXTENT

  def doJump(): Image = {
    var stringJump: String = null
    jumpingExtent += 1
    if (jumpingExtent < Mario.JUMPING_LIMIT) {
      if (getY > Main.scene.heightLimit) {
        setY(getY - Mario.JUMP_OFFSET)
      }
      else jumpingExtent = Mario.JUMPING_LIMIT
    }
    else if (getY + getHeight < Main.scene.floorOffsetY) {
      setY(getY + 1)
    }
    else {
      jumping=false
      jumpingExtent = Mario.JUMPING_EXTENT
    }
    stringJump = if (toRight) Res.IMG_MARIO_SUPER_DX
    else Res.IMG_MARIO_SUPER_SX
    Utils.getImage(stringJump)
  }

  def contact(gameObject: GameObject) {
    if (hitAhead(gameObject) && toRight || hitBack(gameObject) && !toRight) {
      Main.scene.movement=0
      moving=false
    }
    if (hitBelow(gameObject) && jumping) {
      Main.scene.floorOffsetY=gameObject.getY
    }
    else if (!hitBelow(gameObject)) {
      Main.scene.floorOffsetY=Mario.FLOOR_OFFSET_Y_INITIAL
      checkJumpAndHit(gameObject)
    }
  }

  def contact(character: BasicCharacter) {
    if (hitAhead(character) || hitBack(character)) {
      if (character.alive) {
        moving=false
        alive=false
      }
      else {
        alive_$eq(true)
      }
    }
    else if (hitBelow(character)) {
      character.moving=false
      character.alive=false
      Score.getInstance.incrementScore(Mario.HIT_INCREMENT)
      Main.label.setText("  Score:  " + Score.getInstance.getScore)

    }
  }

  private def checkJumpAndHit(gameObject: GameObject) {
    if (!jumping) {
      this.setY(Mario.MARIO_OFFSET_Y_INITIAL)
    }
    if (hitAbove(gameObject)) {
      Main.scene.heightLimit= gameObject.y + gameObject.getHeight
    }
    else if (!hitAbove(gameObject) && !jumping) {
      Main.scene.heightLimit=Mario.HEIGHT_LIMIT
    }
  }

  def contactCoin(coin: Coin): Boolean = hitBack(coin) || hitAbove(coin) || hitAhead(coin) || hitBelow(coin)

}