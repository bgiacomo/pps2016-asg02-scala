package model.characters

import java.awt.Image
import model.GameElement
import model.objects.GameObject
import utils.Res
import utils.Utils

trait Character {
  def walk(name: String, frequency: Int): Image
}

object BasicCharacter {
  private val PROXIMITY_MARGIN = 10
  private val HIT_MARGIN = 5
}

case class BasicCharacter( xCharacter: Int,  yCharacter: Int, widthCharacter: Int,  heightCharacter: Int) extends GameElement(xCharacter, yCharacter, widthCharacter, heightCharacter) with Character {
  var moving = false
  var toRight = true
  var counter = 0
  var alive = true
  private var margin = 0

  override def walk(name: String, frequency: Int): Image = {
    val str: String = Res.IMG_BASE + name + (if (!this.moving || {this.counter += 1; this.counter} % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (this.toRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
    Utils.getImage(str)
  }

  protected def hitAbove(gameObject: GameObject): Boolean = {
     !(this.x + this.width < gameObject.getX + BasicCharacter.HIT_MARGIN || this.x > gameObject.getX + gameObject.getWidth - BasicCharacter.HIT_MARGIN || this.y < gameObject.getY + gameObject.getHeight || this.y > gameObject.getY + gameObject.getHeight + BasicCharacter.HIT_MARGIN)
  }

  protected def hitAhead(character: GameElement): Boolean = {
    if (toRight) {
       !(this.x + this.width < character.getX || this.x + this.width > character.getX + BasicCharacter.HIT_MARGIN || this.y + this.height <= character.getY || this.y >= character.getY + character.getHeight)
    }
    else false
  }

  protected def hitBack(gameObject: GameElement): Boolean = {
     !(this.x > gameObject.getX + gameObject.getWidth || this.x + this.width < gameObject.getX + gameObject.getWidth - BasicCharacter.HIT_MARGIN || this.y + this.height <= gameObject.getY || this.y >= gameObject.getY + gameObject.getHeight)
  }

  protected def hitBelow(gameObject: GameElement): Boolean = {
    if (gameObject.isInstanceOf[GameObject]) {
      this.margin = BasicCharacter.HIT_MARGIN
    }
     !(this.x + this.width < gameObject.getX + margin || this.x > gameObject.getX + gameObject.getWidth - margin || this.y + this.height < gameObject.getY || this.y + this.height > gameObject.getY + margin)
  }

  def isNearby(obj: GameElement): Boolean = {
     (this.x > obj.getX - BasicCharacter.PROXIMITY_MARGIN && this.x < obj.getX + obj.getWidth + BasicCharacter.PROXIMITY_MARGIN) || (this.getX + this.width > obj.getX - BasicCharacter.PROXIMITY_MARGIN && this.x + this.width < obj.getX + obj.getWidth + BasicCharacter.PROXIMITY_MARGIN)
  }


}