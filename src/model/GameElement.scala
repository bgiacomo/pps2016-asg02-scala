package model

import view.Main

/**
  * Created by Giacomo on 11/03/2017.
  */
class GameElement(var x: Int, var y: Int, var width: Int, var height: Int) extends Element {

  def getX: Int = this.x
  def getY: Int = this.y
  def getWidth: Int = this.width
  def getHeight: Int = this.height
  def setX(xCharacter: Int): Unit =this.x = xCharacter
  def setY(yCharacter: Int): Unit =this.y = yCharacter

  override def move(): Unit = {
    if (Main.scene.xPosition >= 0) {
      this.x = this.x - Main.scene.movement
    }
  }
}