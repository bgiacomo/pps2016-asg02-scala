package model.objects

import java.awt.Image
import model.GameElement
import utils.{Res, Utils}

class GameObject(xObject: Int, yObject: Int, val widthObject: Int, val heightObject: Int) extends GameElement(xObject, yObject, widthObject, heightObject) {

  protected var imageObject: Image = _
  final def getImageObject: Image = imageObject
}

object Block {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
}

class Block(x: Int, y: Int) extends GameObject(x,y,Block.WIDTH, Block.HEIGHT) {
  imageObject=Utils.getImage(Res.IMG_BLOCK)
}

object Tunnel {
  private val WIDTH: Int = 43
  private val HEIGHT: Int = 65
}

class Tunnel(x: Int, y: Int) extends GameObject(x, y, Tunnel.WIDTH, Tunnel.HEIGHT) {
  imageObject = Utils.getImage(Res.IMG_TUNNEL)
}