package model.objects

import view.Audio
import utils.Res
import utils.Utils
import java.awt.Image
import java.util.logging.Level
import java.util.logging.Logger

object Coin {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
  private val PAUSE: Int = 10
  private val FLIP_FREQUENCY: Int = 100
  private val LOGGER: Logger = Logger.getLogger(classOf[Audio].getName)
}

class Coin(x: Int,  y: Int) extends GameObject(x, y, Coin.WIDTH, Coin.HEIGHT) with Runnable {
  imageObject = Utils.getImage(Res.IMG_PIECE1)
  private var counter: Int = 0

  final def imageOnMovement: Image = {
     Utils.getImage(if ( {
      this.counter += 1; this.counter
    } % Coin.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  final def run() {
    while (true) {
      {
        this.imageOnMovement
        try { Thread.sleep(Coin.PAUSE) }
        catch {
          case e: InterruptedException => { Coin.LOGGER.log(Level.WARNING, "Coin exception", e) }
        }
      }
    }
  }
}