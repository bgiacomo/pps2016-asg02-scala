package model

/**
  * Created by Giacomo on 11/03/2017.
  */
trait Element {

  def getX: Int
  def getY: Int
  def getWidth: Int
  def getHeight: Int
  def setX(xCharacter: Int)
  def setY(yCharacter: Int)

  def move():Unit
}