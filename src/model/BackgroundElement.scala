package model

import model.objects.GameObject
import java.awt._
import utils.{Res, Utils}


/**
  * Created by Giacomo on 12/03/2017.
  */

class BackgroundElement( xObject: Int,  yObject: Int,  widthObject: Int,  heightObject: Int, val imageBackground: String) extends GameObject(xObject, yObject, widthObject, heightObject) {
  this.image = Utils.getImage(imageBackground)
  var image: Image = _
}

object Castle {
  private val X = 10
  private val Y = 10
}

class Castle extends BackgroundElement(Castle.X, Castle.Y, 0, 0, Res.IMG_CASTLE)

class Background( xObject: Int, yObject: Int) extends BackgroundElement(xObject, yObject, 0, 0, Res.IMG_BACKGROUND)

object FinalCastle {
  private val X = 4850
  private val Y = 145
}

class FinalCastle() extends BackgroundElement(FinalCastle.X, FinalCastle.Y, 0, 0, Res.IMG_CASTLE_FINAL)

object StartPoint {
  private val X = 220
  private val Y = 234
}

class StartPoint() extends BackgroundElement(StartPoint.X, StartPoint.Y, 0, 0, Res.START_ICON)

object Flag {
  private val X = 4650
  private val Y = 115
}

class Flag() extends BackgroundElement(Flag.X, Flag.Y, 0, 0, Res.IMG_FLAG)