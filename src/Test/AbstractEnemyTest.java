package Test;

import model.characters.Mario;
import model.characters.Mushroom;
import model.characters.Turtle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Giacomo on 15/03/2017.
 */
public class AbstractEnemyTest {

    private Turtle turtle;
    private Mushroom mushroom;

    @Before
    public void createEnemies(){
        this.turtle=new Turtle(0,0);
        this.mushroom=new Mushroom(0,0);
    }
    @Test
    public void move() throws Exception {
        turtle.move();
        Assert.assertTrue(turtle.getOffsetX()==1);
        Assert.assertTrue(turtle.getX()==2);
    }

    @Test
    public void contactBetweenTurtleAndMushroom() throws Exception {
        turtle.contact(mushroom);
        Assert.assertTrue(turtle.getOffsetX()==1 || turtle.getOffsetX()==-1);
    }

    @Test
    public void contactWithMario() throws Exception {
        Mario mario = new Mario(0,0);
        turtle.contact(mario);
        Assert.assertTrue(turtle.getOffsetX()==1);
    }


}