package Test;

import org.junit.Assert;
import org.junit.Test;
import view.Score;

/**
 * Created by Giacomo on 15/03/2017.
 */
public class ScoreTest {


    private static final int NUMBER_OF_INCRESEAS = 5;
    private static final int COIN_HIT = 10;

    @Test
    public void incrementScore() throws Exception {

        for (int i = 0; i< NUMBER_OF_INCRESEAS; i++){
            Score.getInstance().incrementScore(COIN_HIT);
        }
        Assert.assertTrue(Score.getInstance().getScore()== NUMBER_OF_INCRESEAS*COIN_HIT);
    }

    @Test
    public void resetScore() throws Exception {
        Score.getInstance().resetScore();
        Assert.assertTrue(Score.getInstance().getScore()==0);

    }

}