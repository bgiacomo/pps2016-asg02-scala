package view

import controller._
import controller.DrawController
import model.objects._
import model.characters._
import java.awt._
import javax.swing._
import model.Background
import model.Castle
import model._
import model.characters.Skull
import scala.collection.mutable.ListBuffer

object Platform {
  private val VICTORY_SOUND = "/resources/audio/stage_clear.wav"
  private val FIRST_BACKGROUND_POSITION: Int = 800
  private val SECOND_BACKGROUND_POSITION: Int = 800
  private val POSITION_TO_CHECK: Int = 4600
  private val INIT_FIRSTBACKGROUND: Int = -50
  private val INIT_SECONDBACKGROUND: Int = 750
  private val X_POSITION: Int = 1
  private val X_POSITION_BEGINNING: Int = 0
  private val INIT_MOVEMENT: Int = 0
  private val FLOOR_OFFSET_Y: Int = 293
  private val HEIGHT_LIMIT: Int = 0
}

class Platform() extends JPanel {

  val objectsHandler: ObjectsHandler = new ObjectsHandler
  val coinHandler: CoinHandler = new CoinHandler
  val firstImageBackground: Background = new Background(0, 0)
  val secondImageBackground: Background = new Background(0, 0)
  var castle: Castle = new Castle
  var flag: Flag = new Flag
  var start: StartPoint = new StartPoint
  var firstBackgroundPosX: Int = Platform.INIT_FIRSTBACKGROUND
  var secondBackgroundPosX: Int = Platform.INIT_SECONDBACKGROUND
  var movement: Int = Platform.INIT_MOVEMENT
  var xPosition: Int = -Platform.X_POSITION
  var floorOffsetY: Int = Platform.FLOOR_OFFSET_Y
  var heightLimit: Int = Platform.HEIGHT_LIMIT
  var mario: Mario = _
  var mushroom: Mushroom = _
  var turtle: Turtle = _
  var skull: Skull = _
  var bird: Bird =_
  var finalCastle: FinalCastle = new FinalCastle
  var drawController: DrawController = _
  var contactController: ContactController = _
  var objectsMoverController: ObjectsMoverController = _
  var enemies: ListBuffer[AbstractEnemy] = new ListBuffer[AbstractEnemy]
  var gameObj: ListBuffer[GameObject] = new ListBuffer[GameObject]
  var audioOccurences=0

  initializeControllers()
  drawCharacters()
  setFocusable(true)
  requestFocusInWindow
  addKeyListener(new Keyboard)
  createEnemiesArray()
  createGameobjArray()

  def updateBackgroundOnMovement() {
    if (this.xPosition >= Platform.X_POSITION_BEGINNING && this.xPosition <= Platform.POSITION_TO_CHECK) {
      this.xPosition = this.xPosition + this.movement
      this.firstBackgroundPosX = this.firstBackgroundPosX - this.movement
      this.secondBackgroundPosX = this.secondBackgroundPosX - this.movement
    }
    if (this.firstBackgroundPosX == Platform.FIRST_BACKGROUND_POSITION || this.firstBackgroundPosX == -Platform.FIRST_BACKGROUND_POSITION) {
      this.firstBackgroundPosX *= -1
    }
    if (this.secondBackgroundPosX == Platform.SECOND_BACKGROUND_POSITION || this.secondBackgroundPosX == -Platform.SECOND_BACKGROUND_POSITION) {
      this.secondBackgroundPosX *= -1
    }
    if(xPosition==Platform.POSITION_TO_CHECK) {
      audioOccurences+=1
      this.playAudioFinalGame()
    }
  }

  override def paintComponent(g: Graphics) {
    super.paintComponent(g)
    val g2: Graphics = g
    contactController.checkContactsWithObjects(objectsHandler.objects, mario, enemies)
    contactController.checkContactWithCoin(coinHandler.coins, mario)
    contactController.checkContactAmongstCharacters(mario,turtle,mushroom,skull,bird)
    this.updateBackgroundOnMovement()
    if (this.xPosition >= Platform.X_POSITION_BEGINNING && this.xPosition <= Platform.POSITION_TO_CHECK) {
      objectsMoverController.moveObjects(objectsHandler.objects)
      objectsMoverController.movePieces(coinHandler.coins)
      objectsMoverController.moveEnemies(enemies)
    }
    this.drawBackground(g2)
    this.drawCharacters(g2)
  }

  private def drawCharacters(g2: Graphics) {
    drawController.drawMario(g2, this.mario)
    enemies foreach((enemy:AbstractEnemy)=>drawController.drawEnemy(g2,enemy))
  }

  private def drawBackground(g2: Graphics) {
    drawController.drawGameObject(g2, firstImageBackground,0)
    drawController.drawGameObject(g2, secondImageBackground,0)
    drawController.drawObjects(g2, objectsHandler.objects)
    drawController.drawPieces(g2, coinHandler.coins)
    gameObj foreach((game:GameObject)=>drawController.drawGameObject(g2,game,this.xPosition))
  }

  private def initializeControllers() {
    this.drawController = new DrawController
    this.contactController = new ContactController
    this.objectsMoverController = new ObjectsMoverController
  }

  private def drawCharacters() {
    mario = new Mario(300, 245)
    mushroom = new Mushroom(800, 263)
    turtle = new Turtle(950, 243)
    skull = new Skull(970, 243)
    bird = new Bird(2800,160)
  }

  private def createEnemiesArray(): Unit ={
    enemies += mushroom
    enemies += turtle
    enemies += skull
    enemies += bird
  }

  private def createGameobjArray(): Unit ={
    gameObj+=castle
    gameObj+=start
    gameObj+=flag
    gameObj+=finalCastle
  }

  private def playAudioFinalGame(): Unit ={
    if(audioOccurences==1) Audio.playSound(Platform.VICTORY_SOUND)
  }

}