package view;

import utils.Res;
import utils.Utils;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class Scene extends JPanel {

    private static final int BACKGROUND_OFFSET_X = -50;
    private static final int BACKGROUND_OFFSET_Y = 0;
    private static final int MARIO_OFFSET_X = 300;
    private static final int MARIO_OFFSET_Y = 245;
    private Image imageBackground;
    private Image imageMario;

    public Scene() {
        super();
        this.imageBackground = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
    }

    @Override
    protected final void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 =  g;

        g2.drawImage(this.imageBackground, BACKGROUND_OFFSET_X, BACKGROUND_OFFSET_Y, null);
        g2.drawImage(imageMario, MARIO_OFFSET_X, MARIO_OFFSET_Y, null);
    }
}
