package view

import javax.swing._
import java.awt._

object Main extends App{

  private val WINDOW_TITLE: String = "Super Mario"

  var scene: Platform = _
  var label: JLabel = _

  createScoreLabel()
  scene = new Platform()
  scene.add(label, BorderLayout.LINE_END)
  new GameWindow(WINDOW_TITLE, scene)
  startTimer()

  private def startTimer(): Unit = {
    val timer: Thread = new Thread(new Refresh())
    timer.start()
  }

  private def createScoreLabel(): Unit = {
    label = new JLabel("  Score:  " + new Score().getScore)
    label.setOpaque(true)
    label.setBackground(Color.green)
  }

}