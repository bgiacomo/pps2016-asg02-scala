package view;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.logging.Logger;
import java.util.logging.Level;


public class Audio {
    private Clip clip;
    private static final Logger LOGGER = Logger.getLogger(Audio.class.getName());

    public Audio(String song) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(song));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Audio exception", e);
        }
    }

    public final void play() {
        clip.start();
    }

    public static void playSound(String song) {
        Audio s = new Audio(song);
        s.play();
    }
}
