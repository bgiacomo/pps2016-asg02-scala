package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private static final String JUMP_SOUND = "/resources/audio/jump.wav";
    private static final int FIRST_BACKGROUND_POSITION = -50;
    private static final int SECOND_BACKGROUND_POSITION = 750;
    private static final int MOVEMENT = 1;
    private static final int BACKGROUND_INITIAL_POSITION_X = 4600;
    private static final int FIRSTBACKGROUND_INITIAL_POSITION_X = 0;

    @Override
    public final void keyPressed(KeyEvent event) {
        if (Main.scene().mario().alive()) {
            switch (event.getKeyCode()){
                case KeyEvent.VK_RIGHT:
                    if (Main.scene().xPosition() == FIRSTBACKGROUND_INITIAL_POSITION_X-MOVEMENT) {
                        this.setBackgroundPosition(FIRSTBACKGROUND_INITIAL_POSITION_X,FIRST_BACKGROUND_POSITION,SECOND_BACKGROUND_POSITION);
                    }
                    this.setMovement(true,true,MOVEMENT);
                    break;
                case KeyEvent.VK_LEFT:
                    if (Main.scene().xPosition() == BACKGROUND_INITIAL_POSITION_X+MOVEMENT) {
                        this.setBackgroundPosition(BACKGROUND_INITIAL_POSITION_X,FIRST_BACKGROUND_POSITION,SECOND_BACKGROUND_POSITION);
                    }
                    this.setMovement(true,false,-MOVEMENT);
                    break;
                case KeyEvent.VK_UP:
                    Main.scene().mario().jumping_$eq(true);
                    Audio.playSound(JUMP_SOUND);
                    break;
                case KeyEvent.VK_R:
                    String[] args = {};
                    Main.main(args);
                    Score.getInstance().resetScore();
                    break;
            }
        }
    }

    private void setMovement(boolean isMoving, boolean isMovingToRight, int movement) {
        Main.scene().mario().moving_$eq(isMoving);
        Main.scene().mario().toRight_$eq(isMovingToRight);
        Main.scene().movement_$eq(movement);
    }

    private void setBackgroundPosition(int xPosition, int firstBackgroundPosition,int secondBackgroundPosition){
        Main.scene().xPosition_$eq(xPosition);
        Main.scene().firstBackgroundPosX_$eq(firstBackgroundPosition);
        Main.scene().secondBackgroundPosX_$eq(secondBackgroundPosition);
    }


    @Override
    public final void keyReleased(KeyEvent event) {
        Main.scene().mario().moving_$eq(false);
        Main.scene().movement_$eq(0);
    }

    @Override
    public void keyTyped(KeyEvent event) {
    }

}
