package view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Giacomo on 10/03/2017.
 */
public class GameWindow extends JFrame {

    private static final int WIDTH = 700;
    private static final int HEIGHT = 360;

    public GameWindow(String title, Platform scene) {
        super(title);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(WIDTH ,HEIGHT);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setAlwaysOnTop(true);
        this.setContentPane(scene);
        this.setVisible(true);
    }
}
